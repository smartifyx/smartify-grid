/**
 * **Created on 26/01/17**
 *
 * smartify-grid/src/components/grid/index.js
 * @author André Timermann <andre.timermann@smarti.io>
 *
 * Componente que representa o datagrid
 * Vue.use(VueMaterial); //Required to boot vue material
 *
 */

import template from "./template.html"

export default {

    template: template,

    data() {

        return{
            columns: ['id', 'name', 'age'],
            tableData: [
                {id: 1, name: "John", age: "20"},
                {id: 2, name: "Jane", age: "24"},
                {id: 3, name: "Susan", age: "16"},
                {id: 4, name: "Chris", age: "55"},
                {id: 5, name: "Dan", age: "40"}
            ],
            options: {
                // see the options API
            },

        }

    },

    props: [],

    created(){


    }
}