/**
 * **Created on 26/01/17**
 *
 * smartify-grid/components/smartifyGrid/index.js
 * @author André Timermann <andre.timermann@smarti.io>
 *
 * Componente Principal para gerar o Grid e seus compomentes auxiliares
 *
 */

import grid from "../grid"

export default {

    template: '<p><grid /></p>',


    /**
     * TODO: Configurar Validações
     */
    props: ['schemas', 'options', 'models'],


    data() {

        return {
            grid
        };

    },

    components: {
        grid

    },

    created(){


    }
}