/*
 * Created by André Timermann on 24/01/17.
 *
 * Arquivo Principal da Aplicação que exporta os principais componentes
 *
 */

import smartifyGrid from "./components/smartifyGrid"

import {ClientTable} from 'vue-tables-2';

export default {
    install(Vue, options){


        Vue.use(ClientTable);

        Vue.component('smartify-grid', smartifyGrid)

    }
}
