import Vue from 'vue'
import App from './App.vue'

import {ServerTable, ClientTable, Event} from 'vue-tables-2';
import template from "../customTemplate/template.jsx"

console.log(template)

let vueTable2Options = {
    skin: ""
};

Vue.use(ClientTable, vueTable2Options, false, template);

new Vue({
    el: '#app',

    render: h => h(App)
});
