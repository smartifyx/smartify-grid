/**
 *  Template Customizado por André Timermann
 */

export default function (h) {


    let rows = require('./template/rows.jsx')(h, this);

    let normalFilter = require('./template/normal-filter.jsx')(h, this);
    let dropdownPagination = require('./template/dropdown-pagination.jsx')(h, this);
    let columnFilters = require('./template/column-filters.jsx')(h, this);
    let footerHeadings = require('./template/footer-headings.jsx')(h, this);
    let noResults = require('./template/no-results.jsx')(h, this);
    let pagination = require('./template/pagination.jsx')(h, this);
    let dropdownPaginationCount = require('./template/dropdown-pagination-count.jsx')(h, this);
    let headings = require('./template/headings.jsx')(h, this);
    let perPage = require('./template/per-page.jsx')(h, this);

    return <div class={"VueTables"}>

        <div class="row">
            <div class="col-md-6">
                {normalFilter}
            </div>
            <div class="col-md-6">
                {dropdownPagination}
                {perPage}
            </div>
        </div>

        <table class={'VueTables__table table ' + this.opts.skin}>
            <thead>
            <tr>
                {headings}
            </tr>
            {columnFilters}
            </thead>
            {footerHeadings}
            <tbody>
            {noResults}
            {rows}
            </tbody>
        </table>

        {pagination}

        {dropdownPaginationCount}

    </div>
}
