import Vue from 'vue'
import App from './App.vue'

//Método 01: Importa Plugin SmartifyForm do próprio diretório
import smartifyGrid from '../../../src'

//Método 02: Importa como Módulo
// import smartifyForm from 'smartify-form'

// Método 03: Importando pelo binário pré-compilado
// import smartifyForm from '../../../dist/build'

Vue.use(smartifyGrid);

new Vue({
    el: '#app',
    render: h => h(App)
})
