# Smartify Grid



## Sobre o vue-tables-2

Este componente é um pouco mais complicado para instalar e configurar, e tem as seguintes caracteristicas
* Ele faz Uso do JSX, portanto será necessário configurar seu projeto para ter suporte a JSX
* Tem Suporte a renderização no servidor
* Tem Suporte ao Vuex


### Requisitos do JSX

    npm install\
      babel-plugin-syntax-jsx\
      babel-plugin-transform-vue-jsx\
      babel-helper-vue-jsx-merge-props\
      --save-dev


### Alterar configuração do babel

Mudar de:

    {
      "presets": [
        ["es2015", { "modules": false }]
      ]
    }

Para:

    {
      "presets": ["es2015"],
      "plugins": ["transform-vue-jsx"]
    }


### Documentação

#### Opções

Opções são definidas em 3 camadas:

1. Padrões de componentes pré-definido.
2. Padrões definidos pelo usuário aplicáveis ​​para a instância **Vue global**. Passado como o segundo parâmetro para a instrução Use.
3. Opções para uma única tabela, passada através das propriedades **options**.

[Rêferencia de Opções](https://jsfiddle.net/matfish2/823jzuzc/embedded/result/)

### Templates

Você pode passar um template customizado para toda a tabela através do quarto parâmetro no

    Vue.use(ClientTable, [options], [useVuex], [customTemplate]);


Você pode encontrar o arquivo de modelo principal em lib/template.jsx. Copiá-lo para o seu projeto e modificar para suas necessidades.

